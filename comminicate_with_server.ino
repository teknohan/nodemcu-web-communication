#include <ESP8266WiFi.h>
     
    const char* ssid     = "wifi-name";
    const char* password = "password";
    const char* host = "www.unigreen-led.com";
    





     
    void setup() {
      pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
      Serial.begin(115200);
      delay(100);
     
      // We start by connecting to a WiFi network
     
      Serial.println();
      Serial.println();
      Serial.print("Connecting to ");
      Serial.println(ssid);
      
      WiFi.begin(ssid, password);
      
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
     
      Serial.println("");
      Serial.println("WiFi connected");  
      Serial.println("IP address: ");
      
     
      Serial.println(WiFi.localIP());
      
    }
     
    int value = 0;



     //########################################### LOOP
    void loop() {
      Serial.println("looping..");
      delay(5000);
      ++value;
     
      Serial.print("connecting to ");
      Serial.println(host);
      
      // Use WiFiClient class to create TCP connections
      WiFiClient client;
      const int httpPort = 80;
      if (!client.connect(host, httpPort)) {
        Serial.println("connection failed");
        return;
      }
      
      // We now create a URI for the request
      String url = "/nodemcu/com.php";
      Serial.print("Requesting URL: ");
      Serial.println(url);
      
      // This will send the request to the server
      client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                   "Host: " + host + "\r\n" + 
                   "Connection: close\r\n\r\n");
      delay(500);
      
      // Read all the lines of the reply from server and print them to Serial
      String total_read;
      while(client.available()){
        String line = client.readStringUntil('\r');
        total_read +=line;
        delay(500);
        //Serial.println(line);
      line ="";
      delay(500);
      }
      

      //Serial.println();
      delay(500);
      Serial.println("closing connection");
      delay(500);
      String haystack =total_read;
      delay(1000);
    
       int ip_result = search(haystack ,"ip");
      if(ip_result >0 && ip_result <1000){
        pass_all_ip();
      }
            
      int blink_result = search(haystack ,"blink");
      if(blink_result >0 && blink_result <1000){
        blinkLED();
      
      } 
      delay(500);
     int sleep_result=search(haystack ,"sleep");
       if(sleep_result>0 && sleep_result<1000 ){
        sleep();
       
      }
     
 
//Serial.println(total_read);
  total_read = "total read cleared ";
 
  //Serial.println(total_read); 
   Serial.println("########################################################################################"); 
}


// ############## SLEEP
static void sleep(){

    Serial.println("sleeping 1 minute");
    delay(10000);
    Serial.print(" .");
    delay(10000);
    Serial.print(".");
    delay(10000);
    Serial.print(".");      
    delay(10000);
    Serial.print(".");
    delay(10000);
    Serial.print(".");
    delay(10000);
    Serial.print(".");            
    
}



//############ BLINK
static void blinkLED(){
  Serial.println("blink");
  digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
  delay(1000);                      // Wait for a second)
  digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
  delay(1000); 
  digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
  delay(1000);  
  digitalWrite(LED_BUILTIN, HIGH); 
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
  delay(1000);                      // Wait for a second)
  digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
  delay(1000); 
  digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
 
}


//############ SEARCH
static int search(String haystack ,String needle){
 int foundpos;
foundpos=-1;
  for (int i = 0; i <= haystack.length() - needle.length(); i++) {
    if (haystack.substring(i,needle.length()+i) == needle) {
      foundpos = i;
    }
  }
  //Serial.print("key word place : ");
 //Serial.println(foundpos);
  int pass_result =foundpos;
  foundpos=-1;
  // Serial.print("foundpos cleaned : ");
   Serial.println(pass_result);
    return pass_result;
     
                     // Wait for two seconds (to demonstrate the active low LED)
 
}

static void pass_all_ip(){
char* host = "www.unigreen-led.com";
// Use WiFiClient class to create TCP connections
      WiFiClient client;
      const int httpPort = 80;
      if (!client.connect(host, httpPort)) {
        Serial.println("connection failed");
        return;
      }
      
      // We now create a URI for the request
      String url = "/nodemcu/reply.php?reply=";
      delay(2000);
      

   IPAddress localAddr = WiFi.localIP();
   url += localAddr[0];
    url += ".";
   url += localAddr[1];
   url += ".";
   url += localAddr[2];
   url += ".";
   url += localAddr[3];
    
      delay(2000);
      Serial.print("Requesting URL: ");
      Serial.println(url);
      
      // This will send the request to the server
      client.print(String("GET ") + url + " HTTP/1.1\r\n" +
                   "Host: " + host + "\r\n" + 
                   "Connection: close\r\n\r\n");
      delay(2000);
      
      // Read all the lines of the reply from server and print them to Serial
   
  
}


